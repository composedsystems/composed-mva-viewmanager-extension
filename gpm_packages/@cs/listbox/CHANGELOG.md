# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security

## [1.0.1] - 2019-02-14
###Changed
- Added package whitelist to install only Source files

## [1.0.0] - 2019-01-23
### Release Note
First release of this component through GPM