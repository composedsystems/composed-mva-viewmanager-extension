# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security

## [0.2.1] - 2020-04-03
### Fixed
- Models no longer automatically stop for all errors (clearing errors in Handle Error.vi was still incorrectly triggering orderly shutdown).

## [0.2.0] - 2020-02-09
### Added
- Model asynchronous orderly shutdown (Models defer stopping until they collect all tracked nested Models).
### Changed
- Logging conditional disable flags changed to REMOVE_FRAMEWORK_LOG and ENABLE_VERBOSE_LOG (both True/False).
- Standard MVA launch, stop, collect logging enabled by default.

## [0.1.1] - 2020-01-22
### Added
- MonitoredMediator extends Mediator to allow zero or more bus monitors.
- IBusMonitor class defines bus monitor interface and must-override method(s).
### Changed
- Made *Launch Nested Model.vim* non-malleable and DD.
- *Write Pre-Addressed Publisher Messages* is now public (for call by ViewModel in full application).

## [0.1.0] - 2020-01-12
### Added
- Option to launch headless (model-only) framework instance for PC or RT applications that do not implement views. Call **Construct Headless MVA Application.vim** to prepare a headless MVA application for launch.